package com.example.myapplication;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment_2#**#} factory method to
 * create an instance of this fragment.
 */
public class Fragment_2 extends Fragment {
    private RecyclerView recyclerView;
    private List<Map<String,Object>> data;
//    private List<String> list_t;
//    private List<String> list_c;
    private Context context;
    private Myadapter myadapter;
    public Fragment_2() {

        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.this_chat, container, false);
        context = view.getContext();

        recyclerView = view.findViewById(R.id.recyclerView);
        data = new ArrayList<Map<String,Object>>();

        String [] title={"小猫","小狗","小鸟","小花","飞机","女孩","男孩","白云","疑问"};
        String [] introduce={"我是一只小猫","我是一只小狗","我是一只小鸟","祖国的花朵","私人飞机","优雅永不过时","永远的骑士","万里晴空","下一个写点啥"};
        String [] I_detail ={"猫，属于猫科动物，分家猫、野猫，是全世界家庭中较为广泛的宠物。家猫的祖先据推测是古埃及的沙漠猫，波斯的波斯猫，已经被人类驯化了3500年（但未像狗一样完全地被驯化）。\n" +
                "一般的猫：头圆、颜面部短，前肢五指，后肢四趾，趾端具锐利而弯曲的爪，爪能伸缩。夜行性。\n" +
                "以伏击的方式猎捕其它动物，大多能攀援上树。猫的趾底有脂肪质肉垫，以免在行走时发出声响，捕猎时也不会惊跑鼠。行进时爪子处于收缩状态，防止爪被磨钝，在捕鼠和攀岩时会伸出来。",
                "狗（拉丁文Canis lupus familiaris）属于脊索动物门、脊椎动物亚门、哺乳纲、真兽亚纲、食肉目、裂脚亚目、犬科动物。中文亦称“犬”，狗分布于世界各地。狗与马、牛、羊、猪、鸡并称“六畜”。有科学家认为狗是由早期人类从灰狼驯化而来，驯养时间在4万年前~1.5万年前。被称为“人类最忠实的朋友”，是饲养率最高的宠物，其寿命大约在12~18年 [1]  。在中国文化中，狗属于十二生肖之一，在十二生肖中的第11位。",
                "鸟，又称作鸟儿。定义：体表被覆羽毛的卵生脊椎动物，鸟的主要特征是：身体呈流线型（纺锤型或梭形），大多数飞翔生活。体表被覆羽毛，一般前肢变成翼（有的种类翼退化）；胸肌发达；直肠短，食量大消化快，即消化系统发达，有助于减轻体重，利于飞行；心脏有两心房和两心室，心搏次数快。体温恒定。呼吸器官除具肺外，还具有多个气囊辅助呼吸，使得鸟类无论在吸气和呼气时，均有氧气通过肺，即双重呼吸。",
                "花是被子植物（被子植物门植物，又称有花植物或开花植物）的繁殖器官，其生物学功能是结合雄性精细胞与雌性卵细胞以产生种子。这一进程始于传粉，然后是受精，从而形成种子并加以传播。对于高等植物而言，种子便是其下一代，而且是各物种在自然分布的主要手段。同一植物上着生的花的组合称为花序。\n" +
                        "（特别注意：本义项介绍的是植物的生殖器官；而花在生活中亦常被指为观赏的开花植物，详见花卉词条或花的另一义项：一类具有观赏价值的植物。）\n" +
                        "裸子植物的花构造较简单，通常无明显的花被，单性，形成雄球花和雌球花，被子植物的花构造复杂多样。所以一般所说的花就是指被子植物的花。",
                "飞机（aeroplane,airplane）是指具有一具或多具发动机的动力装置产生前进的推力或拉力，由机身的固定机翼产生升力，在大气层内飞行的重于空气的航空器。 [1] \n" +
                        "飞机是20世纪初最重大的发明之一，公认由美国人莱特兄弟发明。他们在1903年12月17日进行的飞行作为“第一次重于空气的航空器进行的受控的持续动力飞行”被国际航空联合会（FAI）所认可，同年他们创办了“莱特飞机公司”。自从飞机发明以后，飞机日益成为现代文明不可缺少的工具。它深刻的改变和影响了人们的生活，开启了人们征服蓝天历史。",
                "窈窕淑女，对于女子，我觉得这句话的形容是最恰如其分，窈窕淑女，贤良善良的女子，美丽大方，女孩子就应该是这个样子，这个样子的女孩就是男子所追求的目标应该是男子选来做配偶的人。所以只要一想到女孩的美，就立即想到这两句，窈窕淑女，君子好逑。",
                "形容男孩子的形容词有很多，比如文质彬彬、风流倜傥、一表人才、英姿飒爽、风度翩翩、气宇轩昂、玉树临风、才貌双全、逸群之才、昂藏七尺、彪形大汉、惨绿少年、断袖之宠等",
                "云是大气中的水蒸气遇冷液化成的小水滴或凝华成的小冰晶，所混合组成的漂浮在空中的可见聚合物。\n" +
                        "云是地球上庞大的水循环的有形的结果。太阳照在地球的表面，水蒸发形成水蒸气，一旦水汽过饱和，水分子就会聚集在空气中的微尘（凝结核）周围，由此产生的水滴或冰晶将阳光散射到各个方向，这就产生了云的外观。并且，云可以形成各种的形状，也因在天上的不同高度、形态而分为许多种",
                "①《墨子·公孟》：“若大人行淫暴於国家，进而谏，则谓之不逊；因左右而献谏，则谓之言议，此君子之所疑惑也。”\n" +
                        "②《后汉书·张衡传》：“亲履艰难者知下情，备经险易者达物伪。故能一贯万机，靡所疑惑，百揆允当，庶绩咸熙。”\n" +
                        "③唐刘知几《史通·论赞》：“夫论者所以辩疑惑，释凝滞。”\n" +
                        "《东周列国志》第一百二回：“及秦使捧国书来，欲与魏息兵修好，叩其来意，都是敬慕信陵之语，又接得太子增家信，心中愈加疑惑。”\n" +
                        "④巴金《家》七：“剑云抬起头来看琴的脸，他的脸上现出疑惑的表情。”"};
        int[] image ={R.drawable.cat,R.drawable.dog,R.drawable.birds,R.drawable.flower,R.drawable.plane,R.drawable.girls,R.drawable.boys,R.drawable.yun,R.drawable.idea};
        for (int i=0;i<title.length;i++)
        {
            Map<String,Object> map=new HashMap<String,Object>();
            map.put("标题",title[i]);
            map.put("简介",introduce[i]);
            map.put("头像",image[i]);
            map.put("介绍",I_detail[i]);
            data.add(map);
        }

        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        myadapter = new Myadapter(context,data);
        recyclerView.setAdapter(myadapter);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));//添加下划线
        return view ;
    }
}