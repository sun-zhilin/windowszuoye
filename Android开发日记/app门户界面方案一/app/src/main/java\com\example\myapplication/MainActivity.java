package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //底端菜单栏LinearLayout
    private LinearLayout lchat, lfriend, lfind, lmine;
    //中间页面切换展示FrameLayout
    //底端菜单栏Imageview
    private ImageView ichat, ifriend, ifind, imine,idog;
    private androidx.fragment.app.FragmentManager fragmentManager;
    private Fragment fragment_start = new Fragment1();
    private Fragment fragment_chat = new Fragment_2();
    private Fragment fragment_friend = new Fragment_3();
    private Fragment fragment_find = new Fragment_4();
    private Fragment fragment_mine = new Fragment_5();

    //底端文本 textView
    private TextView wx_xiaoxi;
    private TextView wx_pengyou;
    private TextView wx_faxian;
    private TextView wx_wo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //初始化各个控件
        InitView();

        ichat.setImageResource(R.mipmap.chat_1);
        ichat.setScaleX(0.9f);
        ichat.setScaleY(0.9f);
        ifriend.setImageResource(R.mipmap.linkmenblack);
        ifriend.setScaleY(0.75f);
        ifriend.setScaleX(0.75f);
        ifind.setImageResource(R.mipmap.find);
        ifind.setScaleX(0.825f);
        ifind.setScaleY(0.825f);
        imine.setImageResource(R.mipmap.mine);
        imine.setScaleX(0.75f);
        imine.setScaleY(0.8f);
        //初始化点击触发事件
        InitEvent();

        initFragment();

        androidx.fragment.app.FragmentTransaction transaction=fragmentManager.beginTransaction();
        hideAllFragment(transaction);
        transaction.show(fragment_start);


    }
    private void InitView(){
        lchat = findViewById(R.id.xiaoxi);
        lfriend = findViewById(R.id.lianxiren);
        lfind = findViewById(R.id.faxian);
        lmine = findViewById(R.id.wo);

        ichat = findViewById(R.id.weixin_chat);
        ifriend = findViewById(R.id.weixin_friend);
        ifind = findViewById(R.id.weixin_find);
        imine = findViewById(R.id.weixin_mine);
//        idog = findViewById(R.id.ivdog);

        wx_xiaoxi = findViewById(R.id.tv_xiaoxi);
        wx_pengyou = findViewById(R.id.tv_pengyou);
        wx_faxian = findViewById(R.id.tv_faxian);
        wx_wo = findViewById(R.id.tv_wo);
    }

    private void InitEvent(){
        //设置LinearLayout监听
        lchat.setOnClickListener(this);
        lfriend.setOnClickListener(this);
        lfind.setOnClickListener(this);
        lmine.setOnClickListener(this);

    }

    private void initFragment(){
        fragmentManager=getSupportFragmentManager();
        androidx.fragment.app.FragmentTransaction transaction=fragmentManager.beginTransaction();
        transaction.add(R.id.content_show,fragment_start);
        transaction.add(R.id.content_show,fragment_chat);
        transaction.add(R.id.content_show,fragment_friend);
        transaction.add(R.id.content_show,fragment_find);
        transaction.add(R.id.content_show,fragment_mine);
        transaction.commit();
    }

    private void hideAllFragment(androidx.fragment.app.FragmentTransaction transaction){
        transaction.hide(fragment_start);
        transaction.hide(fragment_chat);
        transaction.hide(fragment_friend);
        transaction.hide(fragment_find);
        transaction.hide(fragment_mine);
        transaction.commit();
    }

    @Override
    public void onClick(View view) {
        androidx.fragment.app.FragmentTransaction transaction=fragmentManager.beginTransaction();
        //每次点击之后，将所有的ImageView和TextView设置为未选中
        hideAllFragment(transaction);
        restartButton();
        //再根据所选，进行跳转页面，并将ImageView和TextView设置为选中
        switch(view.getId()){
            case R.id.xiaoxi:
                transaction.show(fragment_chat);
                ichat.setImageResource(R.mipmap.chat_2);
                wx_xiaoxi.setTextColor(getResources().getColor(R.color.colorTextViewPress));
                break;

            case R.id.lianxiren:
                transaction.show(fragment_friend);
                ifriend.setImageResource(R.mipmap.linkmenblack_2);
                wx_pengyou.setTextColor(getResources().getColor(R.color.colorTextViewPress));
                break;

            case R.id.faxian:
                transaction.show(fragment_find);
                ifind.setImageResource(R.mipmap.find_2);
                wx_faxian.setTextColor(getResources().getColor(R.color.colorTextViewPress));
                break;

            case R.id.wo:
                transaction.show(fragment_mine);
                imine.setImageResource(R.mipmap.mine_2);
                wx_wo.setTextColor(getResources().getColor(R.color.colorTextViewPress));
                break;
        }
    }

    //重新设置ImageView和TextView的状态
    private void restartButton(){
        //设置为未点击状态
        ichat.setImageResource(R.mipmap.chat_1);
        ichat.setScaleX(0.9f);
        ichat.setScaleY(0.9f);
        ifriend.setImageResource(R.mipmap.linkmenblack);
        ifriend.setScaleY(0.75f);
        ifriend.setScaleX(0.75f);
        ifind.setImageResource(R.mipmap.find);
        ifind.setScaleX(0.825f);
        ifind.setScaleY(0.825f);
        imine.setImageResource(R.mipmap.mine);
        imine.setScaleX(0.75f);
        imine.setScaleY(0.8f);

        //设置为灰色
        wx_xiaoxi.setTextColor(getResources().getColor(R.color.black));
        wx_pengyou.setTextColor(getResources().getColor(R.color.black));
        wx_faxian.setTextColor(getResources().getColor(R.color.black));
        wx_wo.setTextColor(getResources().getColor(R.color.black));
    }
}