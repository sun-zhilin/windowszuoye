package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //底端菜单栏LinearLayout
    private LinearLayout lchat, lfriend, lfind, lmine;
    //中间页面切换展示FrameLayout
    private FrameLayout fragment_0,fragment_1,fragment_2,fragment_3,fragment_4;
    //底端菜单栏Imageview
    private ImageView ichat, ifriend, ifind, imine,idog;

    //底端文本 textView
    private TextView wx_xiaoxi;
    private TextView wx_pengyou;
    private TextView wx_faxian;
    private TextView wx_wo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //初始化各个控件
        InitView();

        ichat.setImageResource(R.mipmap.chat_1);
        ichat.setScaleX(0.9f);
        ichat.setScaleY(0.9f);
        ifriend.setImageResource(R.mipmap.linkmenblack);
        ifriend.setScaleY(0.75f);
        ifriend.setScaleX(0.75f);
        ifind.setImageResource(R.mipmap.find);
        ifind.setScaleX(0.825f);
        ifind.setScaleY(0.825f);
        imine.setImageResource(R.mipmap.mine);
        imine.setScaleX(0.75f);
        imine.setScaleY(0.8f);
        fragment_0.setVisibility(View.VISIBLE);
        fragment_1.setVisibility(View.INVISIBLE);
        fragment_2.setVisibility(View.INVISIBLE);
        fragment_3.setVisibility(View.INVISIBLE);
        fragment_4.setVisibility(View.INVISIBLE);

        //初始化点击触发事件
        InitEvent();



    }
    private void InitView(){
        lchat = findViewById(R.id.xiaoxi);
        lfriend = findViewById(R.id.lianxiren);
        lfind = findViewById(R.id.faxian);
        lmine = findViewById(R.id.wo);

        ichat = findViewById(R.id.weixin_chat);
        ifriend = findViewById(R.id.weixin_friend);
        ifind = findViewById(R.id.weixin_find);
        imine = findViewById(R.id.weixin_mine);
        idog = findViewById(R.id.ivdog);

        wx_xiaoxi = findViewById(R.id.tv_xiaoxi);
        wx_pengyou = findViewById(R.id.tv_pengyou);
        wx_faxian = findViewById(R.id.tv_faxian);
        wx_wo = findViewById(R.id.tv_wo);

        fragment_0 =findViewById(R.id.fra_start);
        fragment_1 =findViewById(R.id.fra_xiaoxi);
        fragment_2 =findViewById(R.id.fra_lianxiren);
        fragment_3 =findViewById(R.id.fra_faxian);
        fragment_4 =findViewById(R.id.fra_wo);

    }
    private void InitEvent(){
        //设置LinearLayout监听
        lchat.setOnClickListener(this);
        lfriend.setOnClickListener(this);
        lfind.setOnClickListener(this);
        lmine.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        //每次点击之后，将所有的ImageView和TextView设置为未选中
        restartButton();
        //再根据所选，进行跳转页面，并将ImageView和TextView设置为选中
        switch(view.getId()){
            case R.id.xiaoxi:
                ichat.setImageResource(R.mipmap.chat_2);
                wx_xiaoxi.setTextColor(getResources().getColor(R.color.colorTextViewPress));
                fragment_1.setVisibility(View.VISIBLE);
                break;

            case R.id.lianxiren:
                ifriend.setImageResource(R.mipmap.linkmenblack_2);
                wx_pengyou.setTextColor(getResources().getColor(R.color.colorTextViewPress));
                fragment_2.setVisibility(View.VISIBLE);
                break;

            case R.id.faxian:
                ifind.setImageResource(R.mipmap.find_2);
                wx_faxian.setTextColor(getResources().getColor(R.color.colorTextViewPress));
                fragment_3.setVisibility(View.VISIBLE);
                break;

            case R.id.wo:
                imine.setImageResource(R.mipmap.mine_2);
                wx_wo.setTextColor(getResources().getColor(R.color.colorTextViewPress));
                fragment_4.setVisibility(View.VISIBLE);
                idog.setImageResource(R.mipmap.dog_2);
                idog.setScaleX(1.2f);
                idog.setScaleY(0.95f);
                break;
        }
    }


    //重新设置ImageView和TextView的状态
    private void restartButton(){
        //设置为未点击状态
        ichat.setImageResource(R.mipmap.chat_1);
        ichat.setScaleX(0.9f);
        ichat.setScaleY(0.9f);
        ifriend.setImageResource(R.mipmap.linkmenblack);
        ifriend.setScaleY(0.75f);
        ifriend.setScaleX(0.75f);
        ifind.setImageResource(R.mipmap.find);
        ifind.setScaleX(0.825f);
        ifind.setScaleY(0.825f);
        imine.setImageResource(R.mipmap.mine);
        imine.setScaleX(0.75f);
        imine.setScaleY(0.8f);

        fragment_0.setVisibility(View.INVISIBLE);
        fragment_1.setVisibility(View.INVISIBLE);
        fragment_2.setVisibility(View.INVISIBLE);
        fragment_3.setVisibility(View.INVISIBLE);
        fragment_4.setVisibility(View.INVISIBLE);

        //设置为灰色
        wx_xiaoxi.setTextColor(getResources().getColor(R.color.black));
        wx_pengyou.setTextColor(getResources().getColor(R.color.black));
        wx_faxian.setTextColor(getResources().getColor(R.color.black));
        wx_wo.setTextColor(getResources().getColor(R.color.black));
    }
}