package com.example.myapplication;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class Myadapter extends RecyclerView.Adapter<Myadapter.myviewholder> {

    private List<String> list_tille;
    private List<String> list_context;
    private Context context;
    private View inflater;

    public Myadapter(Context context,List<String> list_tille,List<String> list_context){

        this.list_tille =list_tille;
        this.list_context=list_context;
        this.context=context;
    }
    @Override
    public myviewholder onCreateViewHolder( ViewGroup viewGroup, int viewType) {
        inflater = LayoutInflater.from(context).inflate(R.layout.item,viewGroup,false);
        myviewholder myviewholder=new myviewholder(inflater);
        return myviewholder;
    }

    @Override
    public void onBindViewHolder(myviewholder holder, @SuppressLint("RecyclerView") int position ){

        holder.title.setText(list_tille.get(position));
        holder.content.setText(list_context.get(position));
        holder.tipView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,""+list_context.get(position),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {

        return list_context.size();
    }

    class myviewholder extends RecyclerView.ViewHolder{
        TextView title;
        TextView content;
        ConstraintLayout tipView;
        public myviewholder( View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.textView_1);
            content=itemView.findViewById(R.id.textView_2);
            tipView=itemView.findViewById(R.id.onechat);
        }
    }
}




