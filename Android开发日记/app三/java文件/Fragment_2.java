package com.example.myapplication;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment_2#**#} factory method to
 * create an instance of this fragment.
 */
public class Fragment_2 extends Fragment {
    private RecyclerView recyclerView;
    private List<String> list_t;
    private List<String> list_c;
    private Context context;
    private Myadapter myadapter;
    public Fragment_2() {

        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.this_chat, container, false);
        context = view.getContext();

        recyclerView = view.findViewById(R.id.recyclerView);
        list_t = new ArrayList<String>();
        list_c = new ArrayList<String>();
        for (int i = 0; i <= 10; i++) {
            list_t.add("这是第" + i + "个消息");
        }
        list_c.add("抖音否认进入外卖行业");
        list_c.add("微信测试深度清理功能");
        list_c.add("阿里公布2021达摩院青橙奖获奖名单");
        list_c.add("腾讯看点将改名为信息平台与服务线");
        list_c.add("台积电日本晶圆厂预计2024年投产");
        list_c.add("微软将关闭领英中国服务？领英官方辟谣：不实消息");
        list_c.add("苹果拿走全球手机市场75%利润");
        list_c.add("源代码托管平台GitLab登陆纳斯达克");
        list_c.add("高通回应：Pixel 6系列弃用骁龙芯片改自研");
        list_c.add("谷歌发布Android 12使用条件");
        list_c.add("IntelliJ IDEA 2021.3 EAP 4正式发布");
        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        myadapter = new Myadapter(context, list_t, list_c);
        recyclerView.setAdapter(myadapter);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));//添加下划线
        return view ;
    }
}